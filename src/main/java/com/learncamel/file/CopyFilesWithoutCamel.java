package com.learncamel.file;

import java.io.*;


/**
 * @author : osatoojo
 * @email : oosato@nibss-plc.com.ng, osato.ojo@live.com
 * @date :: 24/02/2020
 */
public class CopyFilesWithoutCamel {
    public static void main(String[] args) throws IOException {

        //step 1: create a file object to read the directories

        File inputDirectory = new File("data/input");
        File outputDirectory = new File("data/output");

        //step 2: read the files from the directory and iterate the files'

        File [] files = inputDirectory.listFiles();

        // step 3 create the output streams and read the file
        for (File source : files){
            if(source.isFile()){
                File dest = new File(outputDirectory.getPath() + File.separator + source.getName());

                // step 3 create the output streams and write the files
                OutputStream outputStream = new FileOutputStream(dest);
                byte[] buffer = new byte[(int) source.length()];
                FileInputStream inputStream = new FileInputStream(source);

                inputStream.read(buffer);
                try {
                    outputStream.write(buffer);
                }

//    OutputStream outputStream = new FileOutputStream()

        //step 4 close the streams
                finally {
                    outputStream.close();
                    inputStream.close();
                }


            }

        }

    }




}
